package com.linealex.jse01;

import com.linealex.jse01.controllers.ConsoleController;
import com.linealex.jse01.controllers.MyConsoleController;

public class App {

    public static void main(String[] args) {
        //Create the implementation of ConsoleController
        ConsoleController controller = new MyConsoleController();
        controller.listenToConsole();
    }
}
