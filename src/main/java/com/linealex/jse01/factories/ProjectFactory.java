package com.linealex.jse01.factories;

public interface ProjectFactory {
    public boolean createProject();

    public void listAllProjects();

    public boolean removeProject();

    public void removeAllProjects();
}
