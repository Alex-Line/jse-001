package com.linealex.jse01.factories;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MyTaskFactory implements TaskFactory {
    private List<String> tasks;

    public MyTaskFactory() {
        this.tasks = new ArrayList<>();
    }

    @Override
    public boolean createTask() {
        System.out.println("ENTER NAME: ");
        Scanner scanner = new Scanner( System.in );
        String nameOfTask = scanner.nextLine();
        if(tasks.contains( nameOfTask )){
            System.out.println("THERE IS TASK " + nameOfTask + "! PLEASE TRY AGAIN \n");
            return false;
        }
        tasks.add( nameOfTask );
        System.out.println("[OK]");
        System.out.println("");
        return true;
    }

    @Override
    public void listAllTasks() {
        System.out.println("[TASK LIST]");
        for (int index = 0; index < tasks.size(); index++) {
            System.out.println( (index+1) + ". " + tasks.get( index ));
        }
        System.out.println("");
    }

    @Override
    public boolean removeTask() {
        System.out.println("ENTER NAME: ");
        Scanner scanner = new Scanner( System.in );
        String nameOfTask = scanner.nextLine();
        if(tasks.contains( nameOfTask )){
            tasks.remove( nameOfTask );
            System.out.println("[OK]");
            System.out.println("");
            return true;
        }
        System.out.println("THERE IS NOT SUCH TASK! PLEASE TRY AGAIN!\n");
        return false;
    }

    @Override
    public void removeAllTasks() {
        tasks.clear();
        System.out.println("[ALL TASKS REMOVED]\n");
    }
}
