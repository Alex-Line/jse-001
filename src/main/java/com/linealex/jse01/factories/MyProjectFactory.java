package com.linealex.jse01.factories;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MyProjectFactory implements ProjectFactory {
    private List<String> projects;

    public MyProjectFactory() {
        this.projects = new ArrayList<>();
    }

    @Override
    public boolean createProject() {
        System.out.println("ENTER NAME: ");
        Scanner scanner = new Scanner( System.in );
        String nameOfProject = scanner.nextLine();
        if(projects.contains( nameOfProject )){
            System.out.println("THERE IS PROJECT " + nameOfProject + "! PLEASE TRY AGAIN \n");
            return false;
        }
        projects.add( nameOfProject );
        System.out.println("[OK]");
        System.out.println("");
        return true;
    }

    @Override
    public void listAllProjects() {
        System.out.println("[PROJECT LIST]");
        for (int index = 0; index < projects.size(); index++) {
            System.out.println((index+1) + ". " + projects.get( index ));
        }
        System.out.println("");
    }

    @Override
    public boolean removeProject() {
        System.out.println("ENTER NAME: ");
        Scanner scanner = new Scanner( System.in );
        String nameOfProject = scanner.nextLine();
        if(projects.contains( nameOfProject )){
            projects.remove( nameOfProject );
            System.out.println("[OK]");
            System.out.println("");
            return true;
        }
        System.out.println("THERE IS NOT SUCH PROJECT! PLEASE TRY AGAIN!\n");
        return false;
    }

    @Override
    public void removeAllProjects() {
        projects.clear();
        System.out.println("[All PROJECTS REMOVED]\n");
    }
}
