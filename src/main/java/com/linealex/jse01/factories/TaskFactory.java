package com.linealex.jse01.factories;

public interface TaskFactory {
    public boolean createTask();

    public void listAllTasks();

    public boolean removeTask();

    public void removeAllTasks();
}
