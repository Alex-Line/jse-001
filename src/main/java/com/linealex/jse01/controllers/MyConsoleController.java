package com.linealex.jse01.controllers;

import com.linealex.jse01.factories.MyProjectFactory;
import com.linealex.jse01.factories.MyTaskFactory;
import com.linealex.jse01.factories.ProjectFactory;
import com.linealex.jse01.factories.TaskFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class MyConsoleController implements ConsoleController {

    private ProjectFactory projectFactory;
    private TaskFactory taskFactory;

    public MyConsoleController() {
        this.projectFactory = new MyProjectFactory();
        this.taskFactory = new MyTaskFactory();
    }

    //Repeatedly scan user's activity via console
    public void listenToConsole(){
        System.out.println("*** Welcome to Task Manager ***");
        while (true) {
            Scanner scanner = new Scanner( System.in );
            switch (scanner.nextLine()) {
                case "project-create":
                    projectFactory.createProject();
                    break;
                case "project-list":
                    projectFactory.listAllProjects();
                    break;
                case "project-remove":
                    projectFactory.removeProject();
                    break;
                case "project-clear":
                    projectFactory.removeAllProjects();
                    break;
                case "select":
                    System.out.println("There is not support yet :(\n");
                    break;
                case "task-create":
                    taskFactory.createTask();
                    break;
                case "task-list":
                    taskFactory.listAllTasks();
                    break;
                case "task-remove":
                    taskFactory.removeTask();
                    break;
                case "task-clear":
                    taskFactory.removeAllTasks();
                    break;
                case "exit":
                    System.exit( 0 );
                    break;
                case "help":
                    showHelp();
                    break;
                default:
                    System.out.println( "UNKNOWN COMMAND! PLEASE TRY AGAIN: \n" );
            }
        }
    }

    //Read help command from file
    private void showHelp(){
        try (BufferedReader reader = new BufferedReader( new FileReader( "src/main/resources/help.txt" ) )) {
            while (reader.ready()){
                System.out.println(reader.readLine());
            }
            System.out.println("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
